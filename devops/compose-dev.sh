#!/usr/bin/env bash
set -e
export DOCKER_DEFAULT_PLATFORM=linux/amd64

docker compose \
    -f docker-compose.yml \
    $@
