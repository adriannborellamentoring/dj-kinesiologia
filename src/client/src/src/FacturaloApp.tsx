import { BrowserRouter, Route, Routes } from 'react-router-dom';
import { LoginPage } from './auth/pages/LoginPage';

export default function FacturaloApp() {
  return (
    <BrowserRouter>
        <div className="container">
            <Routes>
                <Route path='/auth/login' element={<LoginPage />}></Route>
            </Routes>
        </div>
    </BrowserRouter>
  )
}
